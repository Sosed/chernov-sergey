import { Component, OnInit } from '@angular/core';
import { ApplicationState, StoreService } from '../../services/store.service';
import { SlideInterface } from '../../models/hello.model';
import { PROJECTS } from '../../models/mock.data';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  sliderMode = 'full';
  slides: SlideInterface[];

  constructor(protected store: StoreService) {
    this.store.appState.subscribe((state) => {
      this.sliderMode = 'short';
      if (state === ApplicationState.ABOUT) {
        this.slides = [
          {word: 'ОБО МНЕ', time: 5500, effect: 1},
        ];
      } else if (state === ApplicationState.PROJECTS) {
        this.slides = [
          {word: 'ПРОЕКТЫ', time: 5500, effect: 1},
        ];
      } else if (state === ApplicationState.CONTACTS) {
        this.slides = [
          {word: 'КОНТАКТЫ', time: 5500, effect: 1},
        ];
      } else if (state === ApplicationState.PROJECT_DETAIL) {
        this.slides = [
          {word: 'НННННННННННННННННН', time: 5500, effect: 1},
        ];
      } else {
        this.sliderMode = 'full';
        this.slides = [
          {word: 'ПРИВЕТ', time: 2500, effect: 2},
          {word: 'МЕНЯ ЗОВУТ*ЧЕРНОВ СЕРГЕЙ', time: 2500, effect: 2},
          {word: 'Я ФРОНТЕНД*РАЗРАБОТЧИК', time: 2500, effect: 2},
          {word: 'ЕСТЬ*ИНТЕРЕСНЫЙ*ПРОЕКТ?', time: 3500, effect: 1},
          {word: 'А ЕСЛИ НАЙДУ?', time: 3500, effect: 2}
        ];
      }
    });
  }

  ngOnInit(): void {
    this.store.appState.next(ApplicationState.HOME);
    // TODO make elegant
    // Image preloading (Add to cache)
    PROJECTS.forEach(project => {
      const img = new Image();
      img.src = `https://chernov-sergey.s3.eu-west-3.amazonaws.com/${project.img}`;
      img.onload = function() {
        img.classList.add('loaded');
      };
    });

  }
}
