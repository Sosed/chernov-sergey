import { HomeComponent } from './home/home.component';
import { AboutComponent } from './about/about.component';
import { ProjectsComponent } from './projects/projects.component';
import { BasePageComponent } from './base-page.component';
import { ContactsComponent } from './contacts/contacts.component';

export const containers: any[] = [
  BasePageComponent,
  HomeComponent,
  AboutComponent,
  ProjectsComponent,
  ContactsComponent
];

export * from './home/home.component';
export * from './about/about.component';
export * from './projects/projects.component';
export * from './base-page.component';
export * from './contacts/contacts.component';
