import { Component, OnDestroy, OnInit } from '@angular/core';
import { ApplicationState, StoreService } from '../../services/store.service';
import { BasePageComponent } from '../base-page.component';
import { LoadingAnimation } from '../../animations/loading.animation';

@Component({
  selector: 'app-contacts',
  templateUrl: './contacts.component.html',
  animations: LoadingAnimation
})
export class ContactsComponent extends BasePageComponent implements OnInit, OnDestroy {

  constructor(protected store: StoreService) {
    super(store);
  }

  ngOnInit(): void {
    this.store.appState.next(ApplicationState.CONTACTS);
    super.ngOnInit();
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }
}
