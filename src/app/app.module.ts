import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { APP_ROUTES } from './app.routes';
import { AppComponent } from './app.component';

// components
import * as fromComponents from './components';

// containers
import * as fromContainers from './containers';

import { StoreService } from './services/store.service';


@NgModule({
  imports: [
    BrowserModule,
    RouterModule.forRoot(APP_ROUTES, {
      useHash: false
    }),
    BrowserAnimationsModule
  ],
  declarations: [
    AppComponent,
    ...fromComponents.components,
    ...fromContainers.containers,
  ],
  exports: [
    ...fromComponents.components,
    ...fromContainers.containers,
  ],
  providers: [
    StoreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
