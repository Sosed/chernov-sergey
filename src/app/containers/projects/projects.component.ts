import { Component, OnDestroy, OnInit } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';

import { timer } from 'rxjs';

import { ApplicationState, StoreService } from '../../services/store.service';
import { BasePageComponent } from '../base-page.component';
import { projectCategory, ProjectInterface } from '../../models/project.interface';
import { PROJECTS } from '../../models/mock.data';
import { BALL_COLORS } from '../../models/hello.model';
import { PROJECT_ANIMATION } from '../../animations/project.animation';
import { LoadingAnimation } from '../../animations/loading.animation';

function getRand(min = -100, max = 100) {
  return Math.random() * (max - min) + min;
}

@Component({
  selector: 'app-projects',
  templateUrl: './projects.component.html',
  animations: [
    trigger('enterAnimation', [
      transition('void => *', [
        style({opacity: 0}),
        animate('350ms', style({opacity: 1}))
      ]),
      transition('* => void', [
        style({opacity: 1}),
        animate('350ms', style({opacity: 0}))
      ])
    ]),
    ...PROJECT_ANIMATION,
    ...LoadingAnimation
  ]
})
export class ProjectsComponent extends BasePageComponent implements OnInit, OnDestroy {

  projects: ProjectInterface[] = PROJECTS;
  filters: projectCategory[] = ['All', 'Angular', 'JS', 'Yii2', 'TypeScript'];
  currentFilter: projectCategory = 'All';
  countProject = PROJECTS.length;

  constructor(protected store: StoreService) {
    super(store);
  }

  ngOnInit(): void {
    this.store.appState.next(ApplicationState.PROJECTS);
    super.ngOnInit();
    const r = getRand;
    this.projects.forEach(project => {
      project.balls = [];
      for (let i = 0; i < 10; i++) {
        project.balls.push({
          values: [r(), r(), ';', r(), r()].join(' '),
          dur: r(10, 15),
          opacity: r(0.01, .3),
          // tslint:disable-next-line:no-bitwise
          color: BALL_COLORS[~~r(0, BALL_COLORS.length - 1)]
        });
      }
    });
    this.setFilter('All', 300);
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  setFilter(tag: projectCategory, delay = 0) {
    this.currentFilter = tag;
    this.projects = PROJECTS.slice().filter(project => {
      project.state = 'init';
      return project.tags.indexOf(tag) !== -1 || tag === 'All';
    }).map((project, index) => {
      timer(100 * index + delay).subscribe(() => {
        project.state = 'loaded';
      });
      return project;
    });
  }

  isViewProject() {
    return this.store.appState.getValue() === ApplicationState.PROJECT_DETAIL;
  }

}
