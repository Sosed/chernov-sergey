import { Component, OnDestroy, OnInit } from '@angular/core';
import { debounceTime } from 'rxjs/operators';
import { ApplicationState, StoreService } from '../services/store.service';

@Component({
  selector: 'app-base-page',
  template: ``
})
export class BasePageComponent implements OnInit, OnDestroy {

  isLoading = true;
  loadStatus = 'loading';

  constructor(protected store: StoreService) {
  }

  ngOnInit(): void {
    this.store.appState.pipe(
        debounceTime(200)
    ).subscribe(() => {
      this.isLoading = false;
      this.loadStatus = 'loaded';
    });
  }

  ngOnDestroy(): void {
    this.store.appState.next(ApplicationState.HOME);
  }
}
