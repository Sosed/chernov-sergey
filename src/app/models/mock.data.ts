import { ProjectInterface } from './project.interface';

export const PROJECTS: ProjectInterface[] = [
  {
    name: 'Tracker',
    alias: 'sv-log',
    desc: 'Трекер для учёта времени работы над задачами с визуализацией и подробной статистикой.<br/> ' +
        'Главной целью этого проекта является изучение ' +
        '<a href="http://ngxs.io" target="_blank">NGXS</a> (аналог ngrx).<br/>' +
        '<br/>' +
        '<small>Логин: demo; Пароль: 123123</small> ',
    img: 'tracker.jpg',
    link: '//tracker.chernov-sergey.ru',
    linkName: 'tracker.chernov-sergey.ru',
    git: 'http://gitlab.com/sosed/tracker',
    category: 'Experiment',
    tags: ['Experiment', 'Angular', 'TypeScript', 'NGXS', 'Yii2']
  },
  {
    name: 'Carsan',
    alias: 'sv-log',
    desc: 'Интернет магазин для компании Carsan. <br/> Проект интересен тем, что фронтенд реализован на Angular5. ' +
        'Также был сделан упор на мобильную версию магазина.',
    img: 'carsan.jpg',
    link: 'https://carsan25.ru/shop/',
    linkName: 'carsan25.ru',
    category: 'Freelance',
    tags: ['freelance', 'Angular', 'TypeScript', 'SASS', 'Unit-tests']
  },
  {
    name: 'sv-logistic',
    alias: 'sv-log',
    desc: 'Сверстал адаптивный и встроил в CMS сайт для логистической компании SVLOGISTIC. ' +
        'Сайт состоит из двух шаблонов и 12 уникальных страниц.',
    img: 'sv-log.jpg',
    link: 'http://sv-log.ru',
    linkName: 'sv-log.ru',
    git: 'https://github.com/sosed/svlogistic',
    category: 'Freelance',
    tags: ['freelance', 'JS', 'gulp', 'jade', 'SASS', 'WordPress']
  },
  {
    name: 'Tower Defence',
    alias: 'td',
    desc: 'Игра в жанре Tower Defence. Разработал игру в далеком 2013 году на чистом JS. Иногда занимаюсь рефакторингом кода' +
      ' и переводом проекта на новый ES-стандарт. Баланс у игры так себе, но играть интересно.',
    img: 'td.jpg',
    link: '//projects.chernov-sergey.ru/test/td2',
    git: 'https://github.com/sosed/td2',
    category: 'Experiment',
    tags: ['Experiment', 'JS', 'Canvas']
  },
  {
    name: 'illusion.ru',
    alias: 'illusion',
    desc: 'Сайт сети кинотеатров "Иллюзион". <br/>Спроектировал API для продажи билетов онлайн.<br/>' +
        'Рефаторинг и разработка мобильной версии',
    img: 'illuzion.jpg',
    link: 'http://illuzion.ru',
    category: 'Freelance',
    tags: ['freelance', 'gulp', 'bootstrap', 'JS', 'Yii2', 'SASS']
  },
  {
    name: 'Aori',
    alias: 'aori',
    desc: 'Адаптивный landing для компании Aori.',
    img: 'aori.jpg',
    link: '//projects.chernov-sergey.ru/aori/',
    category: 'Axeta',
    tags: ['Axeta', 'bootstrap', 'PhotoShop', 'less']
  },
  {
    name: '2017.illusion.ru',
    alias: 'illusion',
    desc: 'Новогодняя игра от сети кинотеатров "Иллюзион". Реализовал весь цикл разработки.<br/> Проект состоит из трёх частей: <ul>' +
        '<li>Лэндинг для клиента <li>Личный кабинет участника акции <li>Кабинет администратора акции</ul>',
    img: '2017.jpg',
    link: 'http://2017.illusion.ru',
    category: 'Freelance',
    tags: ['freelance', 'gulp', 'bootstrap', 'JS', 'Yii2', 'SASS']
  },
  {
    name: 'tcvn',
    alias: 'tcvn',
    desc: 'Сверстал адаптивный landing для компании TCVN',
    img: 'tcvn.jpg',
    link: '//projects.chernov-sergey.ru/tcvn/event.html',
    category: 'Axeta',
    tags: ['Axeta', 'bootstrap', 'PhotoShop', 'less']
  },
  {
    name: 'Аэропорт Владивостока',
    alias: 'vvo-aero',
    desc: 'Дорабатывал сайт международного Аэропорта Владивосток.<br/>' +
        '<ul><li>Генерация PDF файла с информацией о рейсе <li>Вёрстка</ul>',
    img: 'vvo.jpg',
    link: 'http://vvo.aero/',
    linkName: 'vvo.aero',
    category: 'WebSee',
    tags: ['WebSee', 'MODx', 'PDFlib']
  },
  {
    name: 'IHI',
    alias: 'ihi',
    desc: 'В рамках работы в компании Axeta фиксил баги и добавлял новые модули в тендеровую площадку IHI. ' +
        'Система спроектирована в виде многомодульного SPA.',
    img: 'ihi.png',
    link: 'http://ihi.ru',
    linkName: 'ihi.ru',
    category: 'Axeta',
    tags: ['Axeta', 'Yii2', 'Marionette.js']
  },
  {
    name: 'Размерность фрактала',
    alias: 'julia',
    desc: 'Реализовал программу по расчёту размерности фрактала Жюлиа для одной из дисциплин магистратуры',
    img: 'julia.jpg',
    link: '//projects.chernov-sergey.ru/test/julia',
    category: 'Experiment',
    tags: ['Experiment', 'JS', 'Canvas', 'd3js']
  },
];
