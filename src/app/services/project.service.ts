import { Injectable } from '@angular/core';
import { of, Observable } from 'rxjs';

import { PROJECTS } from '../models/mock.data';
import { ProjectInterface } from '../models/project.interface';
import { StoreService } from './store.service';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  constructor(private store: StoreService) {
  }

  getAll(): Observable<ProjectInterface[]> {
    return of(PROJECTS);
  }

  getOne(alias: string): Observable<ProjectInterface> {
    if (this.store.currentProject.getValue().alias === alias) {
      return this.store.currentProject;
    }
    return of(PROJECTS.find(item => item.alias === alias));
  }

}
