import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-image-canvas',
  templateUrl: './image-canvas.component.html',
  styleUrls: ['./image-canvas.component.scss']
})
export class ImageCanvasComponent implements OnInit {

  @Input() path: string;
  @Input() width = 400;
  @Input() height = 400;
  @ViewChild('canvas', { static: true }) public canvas: ElementRef;
  private ctx: CanvasRenderingContext2D;

  constructor() {
  }

  ngOnInit() {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    canvasEl.width = this.width;
    canvasEl.height = this.height;
    this.ctx = canvasEl.getContext('2d');
  }

}
