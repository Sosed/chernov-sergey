import { Routes } from '@angular/router';
import * as fromContainers from './containers';

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: fromContainers.HomeComponent,
    children: [
      {
        path: 'about',
        component: fromContainers.AboutComponent
      }, {
        path: 'contacts',
        component: fromContainers.ContactsComponent
      }, {
        path: 'projects',
        component: fromContainers.ProjectsComponent,
      }
    ]
  }
];
