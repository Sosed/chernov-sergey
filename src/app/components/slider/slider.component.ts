import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  SimpleChanges,
  ViewChild
} from '@angular/core';
import { Ball, distance, SlideInterface, Vector } from '../../models/hello.model';
import { SYMBOLS } from '../../models/symbol';

const CHART_LENGTH = 5;

@Component({
  selector: 'app-slider',
  styleUrls: ['./slider.component.scss'],
  templateUrl: './slider.component.html'
})
export class SliderComponent implements AfterViewInit, OnChanges {

  @ViewChild('canvas', { static: true })
  canvas: ElementRef;

  @ViewChild('canvasWrapper', { static: true })
  canvasWrapper: ElementRef;

  // setting a width and height for the canvas
  @Input() width = 1100;
  @Input() mode = 'full';
  @Input() height = 800;
  @Input() slides: SlideInterface[] = [];

  currentSlide = 0;

  private ctx: CanvasRenderingContext2D;
  private dots: Ball[] = [];
  private cellWidth: number;
  private countDots: number;
  private timer: number;
  private border: number;
  isLoad = false;
  mouse: Vector = new Vector(0, 0);

  constructor() {
  }

  setSize() {
    if (this.canvasWrapper) {
      this.width = this.canvasWrapper.nativeElement.offsetWidth;
    }
    this.height = this.mode === 'full'
        ? document.documentElement.clientHeight - 200
        : 200;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {
    this.setSize();
    this.init();
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.setSize();
    this.init();
  }

  init() {
    const canvasEl: HTMLCanvasElement = this.canvas.nativeElement;
    canvasEl.width = this.width;
    canvasEl.height = this.height;
    this.ctx = canvasEl.getContext('2d');
    if (this.isLoad) {
      this.currentSlide = -1;
      this.nextSlide();
    }
  }

  ngAfterViewInit(): void {
    this.dots = Array.from({length: 500}, (item, index) => new Ball(0, 0, 10, index));
    this.update();
    this.isLoad = true;
    this.init();
  }

  update() {
    const ctx = this.ctx;
    const now = Date.now();
    ctx.clearRect(0, 0, this.width, this.height);
    ctx.fillStyle = 'rgba(255, 255, 255, 0)';
    ctx.fillRect(0, 0, this.width, this.height);

    if (this.width > 768) {
      this.drawRelation();
    }

    ctx.beginPath();
    let count = this.countDots - 1;
    while (count > -1) {
      this.dots[count].update();
      this.dots[count].draw(ctx);
      count--;
    }
    if (this.timer + 800 < now && this.slides[this.currentSlide].effect === 2) {
      this.dots.forEach(dot => {
        dot.target.set(Vector.rand(this.width, this.height));
      });
    }
    if (this.timer + 800 < now && this.slides[this.currentSlide].effect === 3) {
      this.dots.forEach(dot => {
        dot.target.set(dot.position.x, this.height);
      });
    }
    if (this.timer + 1000 < now) {
      this.nextSlide();
      if (this.slides.length <= 1) {
        this.dots.forEach(dot => {
          dot.setColor();
        });
      }
    }

    requestAnimationFrame(() => this.update());
  }

  nextSlide(n = -1) {
    if (n > -1) {
      this.currentSlide = n;
    } else {
      this.currentSlide++;
    }
    if (this.currentSlide >= this.slides.length) {
      this.currentSlide = 0;
    }
    const slide = this.slides[this.currentSlide];
    const rows = slide.word.split('*');
    let max = 0; // Высчитываем максимальную длину строки в массиве rows.
    this.countDots = 0;

    rows.forEach(word => {
      const len = word.length;
      if (max < len) {
        max = len;
      }
    });

    const fontSize = this.width / (max + 3);
    this.cellWidth = fontSize / CHART_LENGTH;

    rows.forEach((word, rowNumber) => {
      word.split('').map((symbol, index) => {
        const chart = SYMBOLS[symbol];
        const offset = this.getOffset(word.length, index, rows.length, rowNumber);
        chart.forEach((item) => {
          this.dots[this.countDots].target.x = (item % 6) * this.cellWidth + offset.x;
          this.dots[this.countDots].target.y = ~~(item / 6) * this.cellWidth + offset.y;
          this.dots[this.countDots].radius = this.cellWidth / 2;
          this.countDots++;
        });
      });
    });
    this.timer = Date.now() + slide.time;
    this.border = this.cellWidth / 2;
  }

  drawRelation() {
    let i, k, dot, a, b;
    const ctx = this.ctx;
    if (this.border < this.cellWidth) {
      this.border += this.cellWidth / 100;
    }
    const minDist = this.cellWidth + this.cellWidth / 2;
    const mouse = this.mouse;
    for (i = 0; i < this.countDots; i++) {
      dot = this.dots[i];
      a = dot.position;
      const dist = distance(mouse.x, mouse.y, a.x, a.y);
      if (dist < minDist * 2) {
        dot.radius = Math.min((minDist * this.cellWidth / dist), minDist);
      } else {
        dot.radius = this.cellWidth / 2;
      }
      for (k = i + 1; k < this.countDots; k++) {
        b = this.dots[k].position;
        if (distance(a.x, a.y, b.x, b.y) < minDist) {
          ctx.beginPath();
          ctx.lineWidth = this.border;
          const grad = ctx.createLinearGradient(a.x, a.y, b.x, b.y);
          grad.addColorStop(0, this.dots[i].color);
          grad.addColorStop(1, this.dots[k].color);
          ctx.strokeStyle = grad;
          ctx.moveTo(a.x, a.y);
          ctx.lineTo(b.x, b.y);
          ctx.stroke();
          ctx.closePath();
        }
      }
    }
  }

  @HostListener('mousemove', ['$event'])
  onMousemove(event: MouseEvent) {
    const rect = this.canvas.nativeElement.getBoundingClientRect();
    this.mouse.set(event.clientX - rect.left, event.clientY - rect.top);
  }

  /*    removeChar() {
          let count = this.countDots;
          while (count > -1) {
              this.dots[count].target = Vector.rand(this.width, this.height);
              count--;
          }
      }*/

  /*
   * Отступ сверху и справа в пикселях
   *
   * @param {number} length длина слова
   * @param {number} number номер символа в слове
   * @param {number} rows количество строк
   * @param {number} row номер строки
   * @return {x, y} структура
   */
  getOffset(length, number, rows, row) {
    // Высчитываем общую длину слова, учитывая промежутки между символами
    const cellWidth = this.cellWidth;
    const commonWidth = cellWidth * CHART_LENGTH * length + (length - 1) * cellWidth,
        // Высчитываем общую высоты слов, учитывая промежутки между строками
        commonHeight = cellWidth * CHART_LENGTH * rows + (rows - 1) * cellWidth;

    // Высчитываем отступ от начала холста до первого символа.
    const offsetFirstW = (this.width - commonWidth) / 2;
    const offsetFirstH = (this.height - commonHeight) / 2;
    return {
      x: offsetFirstW + number * CHART_LENGTH * cellWidth + number * cellWidth,
      y: offsetFirstH + row * CHART_LENGTH * cellWidth + row * cellWidth * 2 - cellWidth / 2
      // y: height / row - fontSize / row
    };
  }
}
