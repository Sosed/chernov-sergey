import { SliderComponent } from './slider/slider.component';
import { HeaderComponent } from './header/header.component';
import { ImageCanvasComponent } from './image-canvas/image-canvas.component';

export const components: any[] = [
  HeaderComponent,
  SliderComponent,
  ImageCanvasComponent
];

export * from './slider/slider.component';
export * from './header/header.component';
export * from './image-canvas/image-canvas.component';
