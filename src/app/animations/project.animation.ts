import { animate, style, transition, trigger } from '@angular/animations';

export const PROJECT_ANIMATION = [
  trigger(
      'project', [
        transition(':enter', [
          style({opacity: 0.5, transform: 'translateX(40px)'}),
          animate('.1s ease-out', style({opacity: 1, transform: 'translateX(0)'}))
        ]),
        transition(':leave', [
              style({opacity: 1, transform: 'translateX(0)'}),
              animate('.15s ease-out', style({
                opacity: 0.5,
                transform: 'translateX(40px)',
              }))
            ]
        )
      ],
  ),
];
