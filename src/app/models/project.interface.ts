export type projectCategory = 'All' | 'Angular' | 'JS' | 'Yii2' | 'TypeScript' | 'Freelance' | 'Experiment';

export type stateProject = '' | 'init' | 'loading' | 'loaded';

export interface BallInterface {
  values: string;
  dur: number;
  opacity: number;
  color: string;
}

export interface ProjectInterface {
  alias: string;
  name?: string;
  img?: string;
  desc?: string;
  link?: string;
  linkName?: string;
  git?: string;
  category?: string;
  tags?: string[];
  x?: number;
  y?: number;
  balls?: BallInterface[];
  state?: stateProject
}
