export interface SlideInterface {
  word: string;
  time: number;
  effect: number;
}

export class Vector {
  x: number;
  y: number;

  constructor(x, y) {
    this.x = x;
    this.y = y;
  }

  static rand(xMax: number = 600, yMax: number = 600) {
    return new Vector(randomInt(-10, xMax), randomInt(-10, yMax));
  }

  set(x, y = 0) {
    if (typeof x === 'object') {
      y = x.y;
      x = x.x;
    }
    this.x = x || 0;
    this.y = y || 0;
    return this;
  }
}

function randomInt(min, max) {
  return Math.round(min + (Math.random() * (max - min)));
}

export function distance(ax, ay, bx, by) {
  return Math.sqrt(Math.pow(ax - bx, 2) + Math.pow(ay - by, 2));
}

/*
 * Движения объекта из одной точки в другую в 2D пространстве.
 * Функция принимает указатель на объект, тем самым меняя его координаты.
 *
 * @param {x,y} begin - координаты оъекта
 * @param {x,y} end - цель оъекта
 * @param {number} speed - скорость движения
 * @return {bool} достиг ли объект цели
 */
function move(begin, end, speed) {
  const angle = Math.atan2(end.y - begin.y, end.x - begin.x);

  begin.x += speed * Math.cos(angle);
  begin.y += speed * Math.sin(angle);

  return distance(begin.x, begin.y, end.x, end.y) < speed;
}

export const BALL_COLORS: string[] = ['#351330', '#424254', '#CC2A41', '#64908A'];

export class Ball {
  target: Vector;
  position: Vector;
  radius: number;
  color: string;
  id: number;

  constructor(x, y, radius, id) {
    this.id = id;
    this.position = Vector.rand();
    this.target = new Vector(x, y);
    this.radius = radius;
    this.setColor();
  }

  setColor() {
    this.color = BALL_COLORS[randomInt(0, BALL_COLORS.length - 1)];
  }

  update() {
    const speed = distance(this.position.x, this.position.y, this.target.x, this.target.y) / 10;
    if (move(this.position, this.target, speed)) {
      this.position.set(this.target.x, this.target.y);
    }
  }

  draw(ctx: CanvasRenderingContext2D) {
    ctx.fillStyle = this.color;
    ctx.beginPath();
    ctx.arc(this.position.x, this.position.y, this.radius, 0, Math.PI * 2, true);
    ctx.fill();
    ctx.closePath();
  }
}
