import { trigger, state, animate, transition, style } from '@angular/animations';

export const LoadingAnimation = [
  trigger('LoadingAnimation', [
    state('loading', style({
      opacity: 0,
      transform: 'translateY(100px)'
    })),
    state('loaded', style({
      opacity: 1,
      transform: 'translateY(0)'
    })),
    transition('loading => loaded', animate('250ms ease-out'))
  ])
];

