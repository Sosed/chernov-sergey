import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { ProjectInterface } from '../models/project.interface';

export enum ApplicationState {
  HOME = '[ApplicationState] home',
  ABOUT = '[ApplicationState] about',
  CONTACTS = '[ApplicationState] contacts',
  PROJECTS = '[ApplicationState] projects',
  PROJECT_DETAIL = '[ApplicationState] project-detail',
}

@Injectable()
export class StoreService {
  appState = new BehaviorSubject<ApplicationState>(ApplicationState.HOME);
  currentProject = new BehaviorSubject<ProjectInterface>({alias: 'none'});
}
